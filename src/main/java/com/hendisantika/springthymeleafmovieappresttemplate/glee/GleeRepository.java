package com.hendisantika.springthymeleafmovieappresttemplate.glee;

import com.hendisantika.springthymeleafmovieappresttemplate.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-thymeleaf-movie-app-rest-template
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/12/19
 * Time: 07.57
 */
public interface GleeRepository extends PagingAndSortingRepository<Glee, Long>, GleeRepositoryCustom {

    Page<Glee> findAllByUser(User user, Pageable pageable);

}