package com.hendisantika.springthymeleafmovieappresttemplate.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-thymeleaf-movie-app-rest-template
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 7/10/22
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class AngularWebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("{path:^(?!(?:oauth|api|index.html)).*$}/**").setViewName("forward:/index.html");
    }
}
