package com.hendisantika.springthymeleafmovieappresttemplate.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-thymeleaf-movie-app-rest-template
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/12/19
 * Time: 08.15
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/api/signin")
public class SignInController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public SignInController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    User signIn(@RequestParam String email, @RequestParam String password) {
        User user = new User(null, email, passwordEncoder.encode(password), User.Role.USER, 0D, null);
        return userRepository.save(user);
    }

    @PostMapping("/validateEmail")
    Boolean validateEmail(@RequestParam String email) {
        return userRepository.existsByEmail(email);
    }
}
